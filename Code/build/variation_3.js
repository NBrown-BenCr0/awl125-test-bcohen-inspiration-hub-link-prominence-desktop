import cro from "./modules/CROugh"
import "./modules/variation_3.scss"

const test_id = "AWL125"
const variation = "Variation 3"
const custom_dimension = "18"

function init() {
  cro.croLog("init", "Variation Called")
  if (!document.body.classList.contains(`${test_id}_loaded`)) {
    cro.croLog("init", "Body Class Check Passed")
    document.body.classList.add(`${test_id}_loaded`);
    
    cro.gaSendEvent(custom_dimension, test_id, variation, "Loaded");
  }
}

function conditions() {
  return typeof ga !== "undefined"
	  		&& cro.elementExists('.gui-secondary-nav .inspirationhub')
}

cro.pollFor(conditions, init)